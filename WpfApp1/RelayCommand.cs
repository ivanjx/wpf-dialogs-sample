﻿using System;
using System.Windows.Input;

namespace WpfApp1;

public class RelayCommand : ICommand
{
    public event EventHandler? CanExecuteChanged;
    Action? m_action;

    public RelayCommand(Action? action)
    {
        m_action = action;
    }

    public bool CanExecute(object? parameter)
    {
        return true;
    }

    public void Execute(object? parameter)
    {
        m_action?.Invoke();
    }
}
