﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        public RelayCommand AddInfoToastCommand
        {
            get => new RelayCommand(() =>
            {
                App.ToastService.Show(
                    new Toast(
                        "The time is " + DateTime.Now.ToString(),
                        ToastColor.Info,
                        TimeSpan.FromSeconds(3)));
            });
        }

        public RelayCommand AddWarningToastCommand
        {
            get => new RelayCommand(() =>
            {
                App.ToastService.Show(
                    new Toast(
                        "The time is " + DateTime.Now.ToString(),
                        ToastColor.Warning,
                        TimeSpan.FromSeconds(5)));
            });
        }

        public RelayCommand AddErrorToastCommand
        {
            get => new RelayCommand(() =>
            {
                App.ToastService.Show(
                    new Toast(
                        "The time is " + DateTime.Now.ToString(),
                        ToastColor.Error,
                        TimeSpan.FromSeconds(10)));
            });
        }

        public RelayCommand ShowDialogsCommand
        {
            get => new RelayCommand(() => _ = ShowDialogsAsync());
        }

        public MainView()
        {
            InitializeComponent();
            DataContext = this;
        }

        async Task ShowDialogsAsync()
        {
            TaskCompletionSource tcs1 = new TaskCompletionSource();
            TestDialogView dlg1 = new TestDialogView(); // Or use any kind of custom content controls.
            dlg1.Message = "This is the first message";
            dlg1.CloseRequested += tcs1.SetResult;

            TaskCompletionSource tcs2 = new TaskCompletionSource();
            TestDialogView dlg2 = new TestDialogView();
            dlg2.Message = "This is the second message";
            dlg2.CloseRequested += tcs2.SetResult;

            App.DialogService.Show(dlg1);
            await tcs1.Task;
            App.DialogService.Close();

            await Task.Delay(500);

            App.DialogService.Show(dlg2);
            await tcs2.Task;
            App.DialogService.Close();
        }
    }
}
