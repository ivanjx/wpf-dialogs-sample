﻿using System.ComponentModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        Visibility m_dialogVisibility;
        FrameworkElement? m_dialogContent;
        ToastView[] m_toastViews;

        public event PropertyChangedEventHandler? PropertyChanged;

        public Visibility DialogVisibility
        {
            get => m_dialogVisibility;
            set
            {
                m_dialogVisibility = value;
                PropertyChanged?.Invoke(
                    this,
                    new PropertyChangedEventArgs(nameof(DialogVisibility)));
            }
        }

        public FrameworkElement? DialogContent
        {
            get => m_dialogContent;
            set
            {
                m_dialogContent = value;
                PropertyChanged?.Invoke(
                    this,
                    new PropertyChangedEventArgs(nameof(DialogContent)));
            }
        }

        public ToastView[] ToastViews
        {
            get => m_toastViews;
            set
            {
                m_toastViews = value;
                PropertyChanged?.Invoke(
                    this,
                    new PropertyChangedEventArgs(nameof(ToastViews)));
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            m_dialogVisibility = Visibility.Collapsed;
            m_toastViews = [];

            Loaded += HandleLoad;
            Unloaded += HandleUnload;
        }

        void HandleLoad(object sender, RoutedEventArgs e)
        {
            App.DialogService.ShowDialogRequested += HandleShowDialogRequest;
            App.DialogService.CloseDialogRequested += HandleCloseDialogRequest;
            App.ToastService.ToastsChanged += HandleToastsChange;
        }

        void HandleUnload(object sender, RoutedEventArgs e)
        {
            App.DialogService.ShowDialogRequested -= HandleShowDialogRequest;
            App.DialogService.CloseDialogRequested -= HandleCloseDialogRequest;
            App.ToastService.ToastsChanged -= HandleToastsChange;
        }

        void HandleShowDialogRequest(FrameworkElement element)
        {
            if (DialogVisibility == Visibility.Visible)
            {
                throw new Exception("Another dialog is being shown");
            }

            DialogContent = element;
            DialogVisibility = Visibility.Visible;
        }

        void HandleCloseDialogRequest()
        {
            DialogVisibility = Visibility.Collapsed;
            DialogContent = null;
        }

        void HandleToastsChange()
        {
            ToastViews = App.ToastService.Toasts
                .Reverse()
                .Select(x =>
                {
                    ToastView view = new ToastView(x);
                    view.CloseRequested += toast => App.ToastService.Remove(toast);
                    return view;
                })
                .ToArray();
        }

        public class ToastView
        {
            Toast m_toast;

            public event Action<Toast>? CloseRequested;

            public string Text
            {
                get;
            }

            public SolidColorBrush Color
            {
                get;
            }

            public ICommand CloseCommand
            {
                get => new RelayCommand(() =>
                    CloseRequested?.Invoke(m_toast));
            }

            public ToastView(Toast toast)
            {
                m_toast = toast;
                Text = toast.Text;
                Color =
                    toast.Color == ToastColor.Info ? new SolidColorBrush(Colors.AliceBlue) :
                    toast.Color == ToastColor.Warning ? new SolidColorBrush(Colors.LightYellow) :
                    toast.Color == ToastColor.Error ? new SolidColorBrush(Colors.OrangeRed) :
                    throw new Exception("Unknown toast color");
            }
        }
    }
}