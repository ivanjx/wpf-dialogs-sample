﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for TestDialogView.xaml
    /// </summary>
    public partial class TestDialogView : UserControl, INotifyPropertyChanged
    {
        string m_message;

        public event PropertyChangedEventHandler? PropertyChanged;
        public event Action? CloseRequested;

        public string Message
        {
            get => m_message;
            set
            {
                m_message = value;
                PropertyChanged?.Invoke(
                    this,
                    new PropertyChangedEventArgs(Message));
            }
        }

        public ICommand CloseCommand
        {
            get => new RelayCommand(() =>
                CloseRequested?.Invoke());
        }

        public TestDialogView()
        {
            InitializeComponent();
            DataContext = this;
            m_message = string.Empty;
        }
    }
}
