﻿using System.Configuration;
using System.Data;
using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static ToastService ToastService = new ToastService();
        public static DialogService DialogService = new DialogService();
    }

}
