﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfApp1;

public class DialogService
{
    public event Action<FrameworkElement>? ShowDialogRequested;
    public event Action? CloseDialogRequested;

    public void Show(FrameworkElement content)
    {
        ShowDialogRequested?.Invoke(content);
    }

    public void Close()
    {
        CloseDialogRequested?.Invoke();
    }
}
