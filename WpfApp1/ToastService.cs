﻿using System;

namespace WpfApp1;

public enum ToastColor
{
    Info,
    Warning,
    Error
}

public record Toast(
    string Text,
    ToastColor Color,
    TimeSpan Timeout);

public class ToastService
{
    static SemaphoreSlim m_semaphore = new SemaphoreSlim(1, 1);
    Toast[] m_toasts;

    public event Action? ToastsChanged;

    public Toast[] Toasts
    {
        get => m_toasts;
        private set
        {
            m_toasts = value;
            ToastsChanged?.Invoke();
        }
    }

    public ToastService()
    {
        m_toasts = [];
    }

    public void Show(Toast toast)
    {
        Toasts = Toasts
            .Append(toast)
            .ToArray();
        _ = StartTimer(toast);
    }

    public async void Remove(Toast toast)
    {
        await m_semaphore.WaitAsync();

        try
        {
            Toasts = Toasts
                .Where(x => x != toast)
                .ToArray();
        }
        finally
        {
            m_semaphore.Release();
        }
    }

    async Task StartTimer(Toast toast)
    {
        await Task.Delay(toast.Timeout);
        Remove(toast);
    }
}
